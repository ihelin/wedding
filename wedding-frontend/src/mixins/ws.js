export default {
    methods: {
        websocketonopen() {
            console.log("WebSocket连接成功");
        },
        websocketonerror(e) {
            console.log("WebSocket连接发生错误");
        },
        websocketclose(e) {
            console.log("connection closed (" + e + ")");
        }
    }
};
