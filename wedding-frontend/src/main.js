import Vue from 'vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource';
import ElementUI from 'element-ui';

import 'element-ui/lib/theme-chalk/index.css';
import 'src/assets/styles/home.css'
import 'src/assets/styles/wall.css'

Vue.use(VueResource);
Vue.use(ElementUI);

Vue.http.interceptors.push((request, next) => {
    const jwtToken = localStorage.getItem("token");
    if (jwtToken) {
        request.headers.set('Authorization', `Bearer ${jwtToken}`);
    } else {
        delete Vue.http.headers.common.Authorization;
    }
    next(response => {
        if (401 === response.status) {
            router.replace({
                path: '/login'
            });
            return;
        } else if (200 === response.status) {

        } else {

        }
        return response;
    });
});

new Vue({
    router,
    render: h => h(App)
}).$mount('#app');
