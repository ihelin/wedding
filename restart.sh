./mvnw clean package
kill -9 `jps -l | grep 'wedding' |awk '{print $1}'`
nohup java -jar target/wedding-0.0.1-SNAPSHOT.jar --spring.profiles.active=prod &
