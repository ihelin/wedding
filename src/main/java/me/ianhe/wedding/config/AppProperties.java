package me.ianhe.wedding.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author iHelin
 * @date 2019/8/25 14:36
 */
@Component
@ConfigurationProperties(prefix = "app")
public class AppProperties {

    private String jwtSecret = "iHelin";

    private long jwtExpirationInSeconds = 7200;

    private String jwtIssuer = "iHelin";

    public String getJwtSecret() {
        return jwtSecret;
    }

    public AppProperties setJwtSecret(String jwtSecret) {
        this.jwtSecret = jwtSecret;
        return this;
    }

    public long getJwtExpirationInSeconds() {
        return jwtExpirationInSeconds;
    }

    public AppProperties setJwtExpirationInSeconds(long jwtExpirationInSeconds) {
        this.jwtExpirationInSeconds = jwtExpirationInSeconds;
        return this;
    }

    public String getJwtIssuer() {
        return jwtIssuer;
    }

    public AppProperties setJwtIssuer(String jwtIssuer) {
        this.jwtIssuer = jwtIssuer;
        return this;
    }
}
