package me.ianhe.wedding.pojo;

/**
 * @author iHelin
 * @date 2019/8/25 15:44
 */
public class WeddingMsg {

    private String content;
    private String nickname;
    private String headImgUrl;

    public String getContent() {
        return content;
    }

    public WeddingMsg setContent(String content) {
        this.content = content;
        return this;
    }

    public String getNickname() {
        return nickname;
    }

    public WeddingMsg setNickname(String nickname) {
        this.nickname = nickname;
        return this;
    }

    public String getHeadImgUrl() {
        return headImgUrl;
    }

    public WeddingMsg setHeadImgUrl(String headImgUrl) {
        this.headImgUrl = headImgUrl;
        return this;
    }
}
