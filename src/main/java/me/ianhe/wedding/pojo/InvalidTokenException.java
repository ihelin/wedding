package me.ianhe.wedding.pojo;

import org.springframework.security.core.AuthenticationException;

/**
 * @author iHelin
 * @date 2019/8/27 20:30
 */
public class InvalidTokenException extends AuthenticationException {

    public InvalidTokenException(String msg, Throwable t) {
        super(msg, t);
    }

    public InvalidTokenException(String msg) {
        super(msg);
    }
}
