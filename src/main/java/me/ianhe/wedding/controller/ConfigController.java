package me.ianhe.wedding.controller;

import me.ianhe.wedding.entity.SystemUser;
import me.ianhe.wedding.service.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author iHelin
 * @date 2019/8/26 10:16
 */
@RestController
public class ConfigController {

    @Autowired
    private JwtUserDetailsService userDetailsService;

    @GetMapping("/config")
    public Object getConfig() {
        SystemUser userDetails = (SystemUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Integer id = userDetails.getId();
        return userDetailsService.getWeddingDetail(id);
    }
}
