package me.ianhe.wedding.handler;

import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import me.ianhe.wedding.dao.SystemUserMapper;
import me.ianhe.wedding.entity.SystemUser;
import me.ianhe.wedding.enums.UserStatusEnum;
import me.ianhe.wedding.enums.UserTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author iHelin
 * @date 2019/8/26 20:44
 */
@Component
public class SubscribeHandler extends AbstractHandler {

    @Autowired
    private SystemUserMapper systemUserMapper;

    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage,
                                    Map<String, Object> context, WxMpService weixinService,
                                    WxSessionManager sessionManager) throws WxErrorException {

        logger.debug("新关注用户 OPENID: " + wxMessage.getFromUser());

        // 获取微信用户基本信息
        try {
            SystemUser systemUser = systemUserMapper.selectByOpenId(wxMessage.getFromUser());
            if (systemUser == null) {
                WxMpUser userWxInfo = weixinService.getUserService().userInfo(wxMessage.getFromUser());

                systemUser = new SystemUser();
                systemUser.setType(UserTypeEnum.WECHAT_USER.value());
                systemUser.setUserStatue(UserStatusEnum.DEFAULT_USER.value());

                systemUser.setNickname(userWxInfo.getNickname());
                systemUser.setOpenid(userWxInfo.getOpenId());
                systemUser.setCity(userWxInfo.getCity());
                systemUser.setCountry(userWxInfo.getCountry());
                systemUser.setProvince(userWxInfo.getProvince());
                systemUser.setLanguage(userWxInfo.getLanguage());
                systemUser.setQrScene(userWxInfo.getQrScene());
                systemUser.setQrSceneStr(userWxInfo.getQrSceneStr());
                systemUser.setRemark(userWxInfo.getRemark());
                systemUser.setGroupId(userWxInfo.getGroupId());
                systemUser.setHeadImgUrl(userWxInfo.getHeadImgUrl());
                systemUser.setSex(userWxInfo.getSex());
                systemUser.setSexDesc(userWxInfo.getSexDesc());
                systemUser.setSubscribe(userWxInfo.getSubscribe());
                systemUser.setSubscribeTime(userWxInfo.getSubscribeTime());
                systemUser.setSubscribeScene(userWxInfo.getSubscribeScene());
                systemUserMapper.insert(systemUser);
            } else {
                systemUser.setSubscribe(true);
                systemUser.setUserStatue(UserStatusEnum.DEFAULT_USER.value());
                systemUserMapper.updateByPrimaryKeySelective(systemUser);
            }
        } catch (WxErrorException e) {
            if (e.getError().getErrorCode() == 48001) {
                this.logger.info("该公众号没有获取用户信息权限！");
            }
        }

        return WxMpXmlOutMessage.TEXT().content("感谢关注！\n请发送婚礼口令参加婚礼！")
                .fromUser(wxMessage.getToUser())
                .toUser(wxMessage.getFromUser()).build();
    }

}
