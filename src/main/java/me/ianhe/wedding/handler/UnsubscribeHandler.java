package me.ianhe.wedding.handler;

import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import me.ianhe.wedding.dao.SystemUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author iHelin
 * @date 2019/8/26 21:15
 */
@Component
public class UnsubscribeHandler extends AbstractHandler {

    @Autowired
    private SystemUserMapper systemUserMapper;

    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage,
                                    Map<String, Object> context,
                                    WxMpService wxMpService,
                                    WxSessionManager sessionManager) {
        String openId = wxMessage.getFromUser();
        this.logger.info("取消关注用户 OPENID: " + openId);
        systemUserMapper.updateUnSubscribe(openId, false);
        systemUserMapper.deleteUserWeddingByOpenId(openId);
        return null;
    }
}
