package me.ianhe.wedding.dao;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.springframework.stereotype.Repository;

import javax.websocket.Session;
import java.util.Map;
import java.util.Set;

/**
 * @author iHelin
 * @date 2019-07-30 14:19
 */
@Repository
public class MemoryDao {

    /**
     * 记录所有session
     */
    private static final Set<Session> SESSION_SETS = Sets.newCopyOnWriteArraySet();

    /**
     * 记录用户session和婚礼id的对应关系(session,weddingId)
     */
    private static final Map<Session, Integer> WEDDING_FOR_USER = Maps.newConcurrentMap();

    /**
     * 添加session，存入
     *
     * @param session
     * @return
     */
    public boolean addSession(Session session) {
        return SESSION_SETS.add(session);
    }

    /**
     * 删除session
     *
     * @param session
     * @return
     */
    public boolean removeSession(Session session) {
        return SESSION_SETS.remove(session);
    }

    /**
     * 获取所有session
     *
     * @return
     */
    public Set<Session> getAllSessions() {
        return SESSION_SETS;
    }

    public Map<Session, Integer> getAllMap() {
        return WEDDING_FOR_USER;
    }

    /**
     * 获取session数量
     *
     * @return
     */
    public int getSessionCount() {
        return SESSION_SETS.size();
    }

    public Integer addSessionAndUser(Session session, Integer weddingId) {
        return WEDDING_FOR_USER.put(session, weddingId);
    }

    /**
     * 将用户和房间对应关系删除
     *
     * @param session session
     * @return
     */
    public Integer removeUserRoom(Session session) {
        return WEDDING_FOR_USER.remove(session);
    }

    /**
     * 根据session获取房间号
     *
     * @param session session
     * @return
     */
    public Integer getRoomBySession(Session session) {
        return WEDDING_FOR_USER.get(session);
    }

}
