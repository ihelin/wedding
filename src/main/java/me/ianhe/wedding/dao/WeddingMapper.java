package me.ianhe.wedding.dao;

import me.ianhe.wedding.entity.Wedding;

public interface WeddingMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Wedding record);

    int insertSelective(Wedding record);

    Wedding selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Wedding record);

    int updateByPrimaryKey(Wedding record);

    Wedding selectByAdminId(Integer id);

    Wedding selectByCode(String code);
}