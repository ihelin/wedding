package me.ianhe.wedding.dao;

import me.ianhe.wedding.entity.SystemUser;
import org.apache.ibatis.annotations.Param;

public interface SystemUserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SystemUser record);

    int insertSelective(SystemUser record);

    SystemUser selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SystemUser record);

    int updateByPrimaryKey(SystemUser record);

    SystemUser selectByOpenId(String openid);

    SystemUser selectByOpenUsername(String username);

    int updateUnSubscribe(@Param("openid") String openid, @Param("subscribe") Boolean subscribe);

    int insertUserWedding(@Param("userId") Integer userId, @Param("weddingId") Integer weddingId);

    int deleteUserWedding(@Param("userId") Integer userId);

    int deleteUserWeddingByOpenId(@Param("openid") String openid);

    Integer getWeddingIdByUserId(Integer userId);
}