package me.ianhe.wedding.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;

public class Wedding {

    private Integer id;

    private String bridegroom;

    private String bride;

    @JsonIgnore
    private Integer adminId;

    @JsonIgnore
    private Date createTime;

    @JsonIgnore
    private Date updateTime;

    @JsonIgnore
    private String weddingCode;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBridegroom() {
        return bridegroom;
    }

    public void setBridegroom(String bridegroom) {
        this.bridegroom = bridegroom;
    }

    public String getBride() {
        return bride;
    }

    public void setBride(String bride) {
        this.bride = bride;
    }

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getWeddingCode() {
        return weddingCode;
    }

    public Wedding setWeddingCode(String weddingCode) {
        this.weddingCode = weddingCode;
        return this;
    }
}