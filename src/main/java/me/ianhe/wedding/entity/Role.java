package me.ianhe.wedding.entity;

/**
 * @author iHelin
 * @date 2019-08-08 19:13
 */
public class Role {

    private Integer id;

    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
