package me.ianhe.wedding;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author iHelin
 * @date 2019-07-28 16:18
 */
@SpringBootApplication
@MapperScan("me.ianhe.wedding.dao")
public class WeddingApplication {

    public static void main(String[] args) {
        SpringApplication.run(WeddingApplication.class, args);
    }

}
