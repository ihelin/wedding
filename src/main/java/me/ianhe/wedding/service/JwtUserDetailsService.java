package me.ianhe.wedding.service;

import me.ianhe.wedding.dao.SystemUserMapper;
import me.ianhe.wedding.dao.WeddingMapper;
import me.ianhe.wedding.entity.SystemUser;
import me.ianhe.wedding.entity.Wedding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author iHelin
 * @date 2019-08-06 20:21
 */
@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private SystemUserMapper systemUserMapper;

    @Autowired
    private WeddingMapper weddingMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SystemUser systemUser = systemUserMapper.selectByOpenUsername(username);
        if (systemUser == null) {
            throw new UsernameNotFoundException(String.format("未找到名字为'%s'.", username));
        }
        return systemUser;
    }

    public Wedding getWeddingDetail(Integer userId) {
        return weddingMapper.selectByAdminId(userId);
    }

}
