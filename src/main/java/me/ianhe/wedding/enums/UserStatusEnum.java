package me.ianhe.wedding.enums;

/**
 * @author iHelin
 * @date 2019/8/26 10:44
 */
public enum UserStatusEnum {

    /**
     * 默认用户
     */
    DEFAULT_USER(100, "默认用户"),

    /**
     * 绑定用户
     */
    BINDING_USER(200, "绑定用户");

    private final Integer value;
    private final String phrase;

    UserStatusEnum(Integer value, String phrase) {
        this.value = value;
        this.phrase = phrase;
    }

    public Integer value() {
        return value;
    }

    public String getPhrase() {
        return phrase;
    }
}
