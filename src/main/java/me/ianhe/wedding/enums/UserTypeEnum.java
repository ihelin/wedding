package me.ianhe.wedding.enums;

/**
 * @author iHelin
 * @date 2019/8/26 10:44
 */
public enum UserTypeEnum {

    /**
     * 微信用户
     */
    WECHAT_USER(1, "微信用户"),

    /**
     * 系统用户
     */
    SYSTEM_USER(2, "系统用户");

    private final int value;
    private final String phrase;

    UserTypeEnum(int value, String phrase) {
        this.value = value;
        this.phrase = phrase;
    }

    public int value() {
        return value;
    }

    public String getPhrase() {
        return phrase;
    }
}
